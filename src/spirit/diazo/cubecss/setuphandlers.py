# -*- coding: utf-8 -*-
from plone.resource.directory import FilesystemResourceDirectory
from plone.resource.interfaces import IResourceDirectory
from Products.CMFPlone.interfaces import INonInstallable
from zope.component import getSiteManager
from zope.interface import implementer

import os


@implementer(INonInstallable)
class HiddenProfiles(object):
    def getNonInstallableProfiles(self):  # noqa: N802
        """Hide uninstall profile from site-creation and quickinstaller."""
        return [
            "spirit.diazo.cubecss:uninstall",
        ]


def post_install(context):
    """Post install script"""
    # Do something at the end of the installation of this package.


def pre_install(context):
    """Pre install script"""
    # Do something at the beginning of the installation of this package.
    sm = getSiteManager(context=context)
    path = os.path.join(os.path.dirname(__file__), "theme")
    directory = FilesystemResourceDirectory(path, "cubecss-theme")
    sm.registerUtility(
        directory, provided=IResourceDirectory, name="++theme++cubecss-theme"
    )


def uninstall(context):
    """Uninstall script"""
    # Do something at the end of the uninstallation of this package.
    sm = getSiteManager(context=context)
    sm.unregisterUtility(provided=IResourceDirectory, name="++theme++cubecss-theme")
