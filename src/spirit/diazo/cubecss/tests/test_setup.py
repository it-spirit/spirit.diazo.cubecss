# -*- coding: utf-8 -*-
"""Setup tests for this package."""
from plone import api
from plone.app.testing import setRoles
from plone.app.testing import TEST_USER_ID
from spirit.diazo.cubecss.testing import SPIRIT_DIAZO_CUBECSS_INTEGRATION_TESTING

import unittest

try:
    from Products.CMFPlone.utils import get_installer
except ImportError:
    get_installer = None


class TestSetup(unittest.TestCase):
    """Test that spirit.diazo.cubecss is properly installed."""

    layer = SPIRIT_DIAZO_CUBECSS_INTEGRATION_TESTING

    def setUp(self):
        """Custom shared utility setup for tests."""
        self.portal = self.layer["portal"]
        if get_installer:
            self.installer = get_installer(self.portal, self.layer["request"])
        else:
            self.installer = api.portal.get_tool("portal_quickinstaller")

    def test_product_installed(self):
        """Test if spirit.diazo.cubecss is installed."""
        self.assertTrue(self.installer.isProductInstalled("spirit.diazo.cubecss"))

    def test_browserlayer(self):
        """Test that ISpiritDiazoCubecssLayer is registered."""
        from spirit.diazo.cubecss.interfaces import ISpiritDiazoCubecssLayer
        from plone.browserlayer import utils

        self.assertIn(ISpiritDiazoCubecssLayer, utils.registered_layers())


class TestUninstall(unittest.TestCase):

    layer = SPIRIT_DIAZO_CUBECSS_INTEGRATION_TESTING

    def setUp(self):
        self.portal = self.layer["portal"]
        if get_installer:
            self.installer = get_installer(self.portal, self.layer["request"])
        else:
            self.installer = api.portal.get_tool("portal_quickinstaller")
        roles_before = api.user.get_roles(TEST_USER_ID)
        setRoles(self.portal, TEST_USER_ID, ["Manager"])
        self.installer.uninstallProducts(["spirit.diazo.cubecss"])
        setRoles(self.portal, TEST_USER_ID, roles_before)

    def test_product_uninstalled(self):
        """Test if spirit.diazo.cubecss is cleanly uninstalled."""
        self.assertFalse(self.installer.isProductInstalled("spirit.diazo.cubecss"))

    def test_browserlayer_removed(self):
        """Test that ISpiritDiazoCubecssLayer is removed."""
        from spirit.diazo.cubecss.interfaces import ISpiritDiazoCubecssLayer
        from plone.browserlayer import utils

        self.assertNotIn(ISpiritDiazoCubecssLayer, utils.registered_layers())
