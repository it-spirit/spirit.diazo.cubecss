# -*- coding: utf-8 -*-
from plone.app.contenttypes.testing import PLONE_APP_CONTENTTYPES_FIXTURE
from plone.app.robotframework.testing import REMOTE_LIBRARY_BUNDLE_FIXTURE
from plone.app.testing import applyProfile
from plone.app.testing import FunctionalTesting
from plone.app.testing import IntegrationTesting
from plone.app.testing import PloneSandboxLayer
from plone.testing import z2

import spirit.diazo.cubecss


class SpiritDiazoCubecssLayer(PloneSandboxLayer):

    defaultBases = (PLONE_APP_CONTENTTYPES_FIXTURE,)  # noqa: N815

    def setUpZope(self, app, configurationContext):  # noqa: N802, N803
        # Load any other ZCML that is required for your tests.
        # The z3c.autoinclude feature is disabled in the Plone fixture base
        # layer.
        import plone.restapi

        self.loadZCML(package=plone.restapi)
        self.loadZCML(package=spirit.diazo.cubecss)

    def setUpPloneSite(self, portal):  # noqa: N802
        applyProfile(portal, "spirit.diazo.cubecss:default")


SPIRIT_DIAZO_CUBECSS_FIXTURE = SpiritDiazoCubecssLayer()


SPIRIT_DIAZO_CUBECSS_INTEGRATION_TESTING = IntegrationTesting(
    bases=(SPIRIT_DIAZO_CUBECSS_FIXTURE,),
    name="SpiritDiazoCubecssLayer:IntegrationTesting",
)


SPIRIT_DIAZO_CUBECSS_FUNCTIONAL_TESTING = FunctionalTesting(
    bases=(SPIRIT_DIAZO_CUBECSS_FIXTURE,),
    name="SpiritDiazoCubecssLayer:FunctionalTesting",
)


SPIRIT_DIAZO_CUBECSS_ACCEPTANCE_TESTING = FunctionalTesting(
    bases=(
        SPIRIT_DIAZO_CUBECSS_FIXTURE,
        REMOTE_LIBRARY_BUNDLE_FIXTURE,
        z2.ZSERVER_FIXTURE,
    ),
    name="SpiritDiazoCubecssLayer:AcceptanceTesting",
)
