# -*- coding: utf-8 -*-
"""Installer for the spirit.diazo.cubecss package."""

from setuptools import find_packages
from setuptools import setup

long_description = "\n\n".join(
    [
        open("README.rst").read(),
        open("CONTRIBUTORS.rst").read(),
        open("CHANGES.rst").read(),
    ]
)


setup(
    name="spirit.diazo.cubecss",
    version="1.0.dev0",
    description="A diazo theme based on CUBE CSS.",
    long_description=long_description,
    # Get more from https://pypi.org/classifiers/
    classifiers=[
        "Environment :: Web Environment",
        "Framework :: Plone",
        "Framework :: Plone :: Addon",
        "Framework :: Plone :: 5.2",
        "Programming Language :: Python",
        "Programming Language :: Python :: 2.7",
        "Programming Language :: Python :: 3.6",
        "Programming Language :: Python :: 3.7",
        "Operating System :: OS Independent",
        "License :: OSI Approved :: GNU General Public License v2 (GPLv2)",
    ],
    keywords="Python Plone",
    author="Thomas Massmann",
    author_email="thomas.massmann@it-spir.it",
    url="https://github.com/collective/spirit.diazo.cubecss",
    project_urls={
        "PyPI": "https://pypi.python.org/pypi/spirit.diazo.cubecss",
        "Source": "https://github.com/collective/spirit.diazo.cubecss",
        "Tracker": "https://github.com/collective/spirit.diazo.cubecss/issues",
        # 'Documentation': 'https://spirit.diazo.cubecss.readthedocs.io/en/latest/',
    },
    license="GPL version 2",
    packages=find_packages("src", exclude=["ez_setup"]),
    namespace_packages=["spirit", "spirit.diazo"],
    package_dir={"": "src"},
    include_package_data=True,
    zip_safe=False,
    python_requires="==2.7, >=3.6",
    install_requires=[
        "setuptools",
        # -*- Extra requirements: -*-
        "plone.api>=1.8.4",
        "plone.app.dexterity",
        "plone.restapi",
        "collective.themefragments",
        "z3c.jbot",
    ],
    extras_require={
        "test": [
            "plone.app.testing",
            # Plone KGS does not use this version, because it would break
            # Remove if your package shall be part of coredev.
            # plone_coredev tests as of 2016-04-01.
            "plone.app.contenttypes",
            "plone.app.robotframework[debug]",
            "plone.testing>=5.0.0",
        ],
    },
    entry_points="""
    [z3c.autoinclude.plugin]
    target = plone
    [console_scripts]
    update_locale = spirit.diazo.cubecss.locales.update:update_locale
    """,
)
